import React from 'react';
import { StyleSheet, Text, View,TextInput } from 'react-native';
import Button from 'apsl-react-native-button'

export default class FirstScreen extends React.Component{


    clickToButton=()=>{
        this.props.navigation.navigate("ViewImage");
    };
    render(){

        return(
        <View style={styles.firstContainer}>

            <View style={styles.borderInFirstScreen}>

                <Text style={styles.text}>
                    Login
                </Text>

                <TextInput style={styles.input}
                 placeholder = "..."
                />

                <Text style={styles.text}>
                    Password
                </Text>

                <TextInput style={styles.input}
                placeholder = "..."
                />

                <Button style={styles.but} onPress={this.clickToButton}>
                    <Text style={styles.buttonText}>
                        Вход
                    </Text>
                </Button>


            </View>

        </View>

        )
    }
}
const styles = StyleSheet.create({
    firstContainer:{
        width:"100%",
        height:"100%",
        justifyContent:"center",
        alignItems:'center',
        backgroundColor:"aliceblue",
    },
    borderInFirstScreen:{
        width: 300,
        height: 400,
        flexDirection:"column",
        justifyContent:'center',
        alignItems: 'center',
        marginLeft:"auto",
        marginRight:"auto",
        borderWidth :5,
        borderColor:"black",
        borderRadius:20,
        backgroundColor: 'azure',
    },
    text:{
        fontSize:17,
        marginRight: "35%"
    },
    input:{
        width:200,
        height:50,
        borderRadius: 10,
        borderWidth: 1,
        backgroundColor:"aliceblue",
        marginBottom: "5%",
        fontSize:25
    },
    but:{
        marginTop:"15%",
        width:180,
        height:70,
         justifyContent:"center",
        marginLeft:"auto",
        marginRight:"auto",
        borderWidth: 3,
        borderRadius: 10,
        backgroundColor: 'aliceblue',
    },
    buttonText:{
        color:"red",
        fontSize:35,
        fontWeight: "bold"
    }
});