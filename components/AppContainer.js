import { createAppContainer, createStackNavigator } from 'react-navigation';
import FirstScreen from "./FirstScreen";
import SecondScreen from "./SecondScreen";
// you can also import from @react-navigation/native

const AppNavigator = createStackNavigator(
{
    Home: FirstScreen,
    ViewImage: SecondScreen,
},
{
    headerMode: 'none',
},
);

const AppContainer = createAppContainer(AppNavigator);

// Now AppContainer is the main component for React to render

export default AppContainer;