import React from 'react';
import { StyleSheet, Text, View,TextInput,Image } from 'react-native';
import Button from 'apsl-react-native-button'
import { AntDesign } from '@expo/vector-icons';

export default class SecondScreen extends React.Component {

    constructor(props){
        super(props);
        this._isMounted=false;
    }
    componentDidMount() {
        this._isMounted=true;

        window.interval=setInterval(() => {
            fetch("https://dog.ceo/api/breeds/image/random")
                .then(response => response.json())
                .then(response => {
                    if(this._isMounted)
                        this.setState({image: response.message, loading: false})
                });
        }, this.state.inputValue * 1000);
    }
    componentWillUpdate(nextProps,nextState){
        if(this.state.inputValue!==nextState.inputValue){
            clearInterval(interval);
            window.interval=setInterval(() => {
                fetch("https://dog.ceo/api/breeds/image/random")
                    .then(response => response.json())
                    .then(response => {
                        if(this._isMounted)
                            this.setState({image: response.message,loading:false})
                    });
            }, nextState.inputValue * 1000);
        }
    }
    componentWillUnmount(){
        this._isMounted=false;
    }
    state={
        loading:true,
        inputValue:3,
        image:''
    };

    clickToButton=()=>{
        this.props.navigation.navigate("Home")
    };

    increaseValue=()=>{
        if(this.state.inputValue===10)
            return this.setState({inputValue:10});
        this.setState({inputValue:this.state.inputValue+1})
    };

    lessenValue=()=>{
        if(this.state.inputValue===1)
            return this.setState({inputValue:1});
        this.setState({inputValue:this.state.inputValue-1})
    };


    render(){


        return(
            <View style={styles.containerSecondScreen}>
                <View style={styles.blockInputAndButtons}>

                    <View style={styles.inputsContainer} editable={false}>

                        <Text style={styles.textInContainer}>
                            {this.state.inputValue}
                        </Text>

                        <View style={styles.blockButtons}>
                            <AntDesign style={styles.antDesign1} name="upcircle" size={30} onPress={this.increaseValue}/>
                            <AntDesign style={styles.antDesign2} name="downcircle" size={30} onPress={this.lessenValue}/>
                        </View>
                    </View>

                </View>
                {this.state.loading?<Text>Loading...</Text>:<Image source={{uri:this.state.image}} style={{width:"90%",height:370}}/>}
                <Button style={styles.but} onPress={this.clickToButton}>
                    <Text style={styles.buttonText}>
                        Выход
                    </Text>
                </Button>
            </View>

        )

    }
}

const styles = StyleSheet.create({
    containerSecondScreen:{
        width:"100%",
        height:"100%",
        flexDirection:"column",
        justifyContent:"center",
        alignItems:'center',
        backgroundColor:"aliceblue",
    },
    blockInputAndButtons:{
        marginTop:"10%",
        flexDirection:"row",
        justifyContent:"center",
        alignItems:'center',
    },
    inputsContainer:{
        width:100,
        height: 80,
        borderWidth: 2,
        flexDirection:"row",
    },
    textInContainer:{
        color:"red",
        marginTop:"auto",
        marginLeft:"auto",
        marginRight:"auto",
        marginBottom:"auto",
        fontSize:30,
        fontWeight: "bold",

    },
    blockButtons:{
        padding:"5%",
        flexDirection:"column",
        marginLeft:"auto",
        borderLeftWidth: 2,
        justifyContent:"center",

    },
    antDesign1:{
        marginBottom:"auto"
    },
    antDesign2:{
        marginTop:"auto"
    },
    but:{
        marginTop:"5%",
        width:180,
        height:70,
        justifyContent:"center",
        marginLeft:"auto",
        marginRight:"auto",
        borderWidth: 3,
        borderRadius: 10,
        backgroundColor: 'aliceblue',
        textAlign: "center"
    },
    buttonText:{
        color:"red",
        fontSize:35,
        fontWeight: "bold",
    }

});